import re
from pyspark import SparkConf, SparkContext
from pyspark.mllib.clustering import KMeans
import sys

CONFIG = {}

CONFIG['totalLocation'] = 5
CONFIG['totalFoodAndBeverages'] = 15

def parseLine(line):
    fields = line.lower().split('\t')
    try:
        latitude = float(fields[21])
    except:
        latitude = 0
    try:
        longitude = float(fields[22])
    except:
        longitude = 0

    optionalText = fields[7]
    return latitude, longitude, optionalText


# Parsing Makanan dan Minuman
# 1. Hilangkan karakter `"`
# 2. Hilangkan kata Cold Truck dengan menghapus kata sebelum `:`
# 3. Split data dengan karakter `,`
# 4. Return hasil dari makanan dan minuman yang ada pada tiap tokonya
def parseFoodAndBeverages(foodCol):
	optionalText = foodCol
	optionalText = optionalText.replace('"', "")
	if (": " in optionalText):
		optionalText = optionalText.split(": ")[1]

	temp = optionalText.split(',')
	result = [x.strip() for x in temp]
	return result

# Function for printing each element in RDD
def println(x):
    print str(x)

conf = SparkConf().setMaster("local").setAppName("BestFoodAndBeverages")
sc = SparkContext(conf = conf)

input = sc.textFile("Mobile_Food_Schedule tab_delimited.txt")

parsedLine = input.map(parseLine)

#1 - Get List of Recommended Place
position = parsedLine.map(lambda x: (x[0], x[1])).collect()
model = KMeans.train(sc.parallelize(position), CONFIG['totalLocation'], maxIterations=5, runs=30, initializationMode="k-means")

print "--------------------------------------------------------------------------------"
print "Rekomendasi Lokasi : "
for i in range (0, CONFIG['totalLocation']-1):
	print "Lokasi #" + str(i+1) + " : " + str(model.centers[i])
print "--------------------------------------------------------------------------------"


#2 - Get List of Food
foodList = parsedLine.map(lambda x: x[2])
foodListFlat = foodList.flatMap(parseFoodAndBeverages)
foodListCount = foodListFlat.map(lambda x: (x,1)).reduceByKey(lambda x, y:(x + y))
foodListCountSorted = foodListCount.map(lambda (x,y): (-y,x)).sortByKey().take(CONFIG['totalFoodAndBeverages'])

results = foodListCountSorted

print "--------------------------------------------------------------------------------"
print "Rekomendasi Makanan & Minuman : "
i = 0
for result in results:
    count = str(result[0] * -1)
    food = str(result[1])
    i = i + 1
    print "#" + str(i) + " : " + food + ": " + (count)
print "--------------------------------------------------------------------------------"